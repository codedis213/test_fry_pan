from django.db import models

# Create your models here.
from mongoengine import *
from blog.settings import DBNAME

connect(DBNAME)

class Recipecollection(Document):
	title = StringField(max_length=100, required=True)
	domain = StringField(max_length=100, required=True)
	itemurl = URLField()
	categoriestags = ListField(StringField(max_length=100))
	author = StringField(max_length=100, required=True)
	authorlink = URLField()
	serves = StringField(max_length=50, required=True)
	imglink = URLField()
	ingredients = StringField()