from django.shortcuts import render_to_response
from django.template import RequestContext
from models import Recipecollection
import datetime
from django.http import HttpResponse
import json
from django.views.decorators.csrf import csrf_exempt


def index(request):
    if request.method == 'POST':
       # save new post
      domain = request.POST['domain']
      post = Recipecollection(domain=domain)
      post.title = request.POST['title']
      post.itemurl = request.POST['itemurl']
      post.categoriestags = [cate.encode("ascii", "ignore").strip() for cate in request.POST['categoriestags'].split(",")]
      post.author = request.POST["author"]
      post.authorlink = request.POST["authorlink"]
      post.serves = request.POST["serves"]
      post.imglink = request.POST["imglink"]
      post.ingredients = request.POST["ingredients"]
      post.save()

    # Get all posts from DB
    posts = Recipecollection.objects 
    return render_to_response('blogapp/index.html', {'Posts': posts},
                              context_instance=RequestContext(request))


def update(request):
    id = eval("request." + request.method + "['id']")
    post = Recipecollection.objects(id=id)[0]
    
    if request.method == 'POST':
        # update field values and save to mongo
        post.domain = request.POST['domain']
        post.title = request.POST['title']
        post.itemurl = request.POST['itemurl']
        post.categoriestags = [cate.encode("ascii", "ignore").strip() for cate in request.POST['categoriestags'].split(",")]
        post.author = request.POST["author"]
        post.authorlink = request.POST["authorlink"]
        post.serves = request.POST["serves"]
        post.imglink = request.POST["imglink"]
        post.ingredients = request.POST["ingredients"]
        post.save()
        template = 'blogapp/index.html'
        params = {'Posts': Recipecollection.objects} 

    elif request.method == 'GET':
        template = 'blogapp/update.html'
        params = {'post':post}
   
    return render_to_response(template, params, context_instance=RequestContext(request))
                              

def delete(request):
    id = eval("request." + request.method + "['id']")

    if request.method == 'POST':
        post = Recipecollection.objects(id=id)[0]
        post.delete() 
        template = 'blogapp/index.html'
        params = {'Posts': Recipecollection.objects} 
    elif request.method == 'GET':
        template = 'blogapp/delete.html'
        params = { 'id': id } 

    return render_to_response(template, params, context_instance=RequestContext(request))


@csrf_exempt
def search(request):
  term = request.POST.get("term", "")
  query = {"$or":[{ "title": { "$regex": r'^%s.*'%(term), "$options": "si" }}, {"author": { "$regex": r'^%s.*'%(term), "$options": "si" } }]}
  r_record = Recipecollection.objects(__raw__=query)
  params = {'Posts': r_record} 
  template = "blogapp/blog_search_temp.html"
  return render_to_response(template, params, context_instance=RequestContext(request))