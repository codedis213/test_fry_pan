from django.conf.urls import patterns, include, url

from . import views

app_name = 'blogapp'
urlpatterns = patterns('',
    url(r'^$', views.index, name="index"),
    url(r'^update/$', views.update, name="update"),
    url(r'^delete/$', views.delete, name="delete"),
    url(r'^search/$', views.search, name="search"),
)